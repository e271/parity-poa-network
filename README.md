
# parity PoA network

Using `vagrant up` one can spin up a virtual machine on vagrant available 
providers and setup a 3-authority PoA network. For simplicity the nodes will 
all be running on the same machine.


## accounts

Without further automation ansible will finish with systemd unit file in place 
for the parity node but will not have the user accounts set up. This can be 
done with the following steps according to 
https://wiki.parity.io/Demo-PoA-tutorial
After setting up the accounts on the nodes ansible variables can be specified 
in order to rebuild the proper configuration files.

### node0

```sh
# systemctl start parity@node0
```


After checking whether parity is up (`systemctl status parity@node0`), set up account on node0:
```sh
$ curl --data '{"jsonrpc":"2.0","method":"parity_newAccountFromPhrase","params":["node0", "node0"],"id":0}' -H "Content-Type: application/json" -X POST localhost:8540
{"jsonrpc":"2.0","result":"0x00bd138abd70e2f00903268f3db08f2d25677c9e","id":0}
```

set up user account on node0:
```sh
$ curl --data '{"jsonrpc":"2.0","method":"parity_newAccountFromPhrase","params":["user", "user"],"id":0}' -H "Content-Type: application/json" -X POST localhost:8540
{"jsonrpc":"2.0","result":"0x004ec07d2329997267ec62b4166639513386f32e","id":0}
```

### node1

```sh
# systemctl start parity@node1
```

```sh
$ curl --data '{"jsonrpc":"2.0","method":"parity_newAccountFromPhrase","params":["node1", "node1"],"id":0}' -H "Content-Type: application/json" -X POST localhost:8541
{"jsonrpc":"2.0","result":"0x00aa39d30f0d20ff03a22ccfc30b7efbfca597c2","id":0}
```

### node 2

```sh
# systemctl start parity@node2
```

```sh
$ curl --data '{"jsonrpc":"2.0","method":"parity_newAccountFromPhrase","params":["node2", "node2"],"id":0}' -H "Content-Type: application/json" -X POST localhost:8542
{"jsonrpc":"2.0","result":"0x002e28950558fbede1a9675cb113f0bd20912019","id":0}
```


## complete chain specification

Having the accounts created as noted above:

- node0 has authority account `0x00bd138abd70e2f00903268f3db08f2d25677c9e`
  with user account `0x004ec07d2329997267ec62b4166639513386f32e`
- node1 has authority account `0x00aa39d30f0d20ff03a22ccfc30b7efbfca597c2`
- node2 has authority account `0x002e28950558fbede1a9675cb113f0bd20912019`


The authority accounts can now be added to the list of validators (which is 
already done in this repo). And apart from that the user account has been 
given some initial balance. After account creation and modifications of the 
configuration files the parity nodes will be restarted (if necessary run 
ansible resp vagrant provision twice for that).



## connecting the nodes

At first the *enode addresses* of node 0 can be retrieved:
```sh
$ curl --data '{"jsonrpc":"2.0","method":"parity_enode","params":[],"id":0}' -H "Content-Type: application/json" -X POST localhost:8540
{"jsonrpc":"2.0","result":"enode://877109edf3c12bd42099569384529f931c185399dffbcc300b35c79eff303b1c7c4d7517004184882282edf9c8070f77184020c1b33cdc906d71d1052755793b@167.99.251.238:30300","id":0}
```

Then node0 can be connected to node1 and node2:
```sh
$ curl --data '{"jsonrpc":"2.0","method":"parity_addReservedPeer","params":["enode://RESULT"],"id":0}' -H "Content-Type: application/json" -X POST localhost:8541
{"jsonrpc":"2.0","result":true,"id":0}
$ curl --data '{"jsonrpc":"2.0","method":"parity_addReservedPeer","params":["enode://RESULT"],"id":0}' -H "Content-Type: application/json" -X POST localhost:8542
{"jsonrpc":"2.0","result":true,"id":0}
```

The nodes connection can be seen in their standart output e.g.
```sh
journalctl -u parity@node0
```
which should state *2/25 peers* for every parity node.



## conclusion

Once the nodes are connected transactions can be made as stated in 
https://wiki.parity.io/Demo-PoA-tutorial. Further improvement could be full 
automation of this setup, which might have been considered overengineered for
this proof of concept. Monitoring of the setup is achieved using 
https://github.com/cubedro/eth-netstats which can be viewed pointing a web 
browser at http://localhost:3000 .

![](ethereum-network-statspng.png)







